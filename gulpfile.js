'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const nunjucksRender = require('gulp-nunjucks-render');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
  return gulp.src('./*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./*.scss', ['sass']);
});

gulp.task('nunjucks', function(){
  return gulp.src('./templates/*.nunjucks')
  .pipe(nunjucksRender({
    path: ['./templates/']
  }))
  .pipe(gulp.dest('dist'));
});

gulp.task('nunjucks:watch', function(){
  gulp.watch('./templates/*.nunjucks', ['nunjucks'])
});

gulp.task('watch', ['nunjucks:watch', 'sass:watch']);
